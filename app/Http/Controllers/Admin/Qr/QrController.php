<?php

namespace App\Http\Controllers\Admin\Qr;

use Redirect;
use App\Http\Requests;
use App\Models\Qr;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;

class QrController extends AdminController
{
    public function index()
    {
        save_resource_url();

        return $this->view('qr.index')->with('items', Qr::all());
    }
    //delete

    public function destroy(Qr $qr)
    {
        $this->deleteEntry($qr, request());
        log_activity('Qr Deleted', 'A Qr was successfully deleted', $qr);
        return redirect_to_resource();
    }

    public function create()
    {
        return $this->view('qr.create_edit');
    }

    public function edit(Qr $qr)
    {
        return $this->view('qr.create_edit')->with('item', $qr);
    }
    public function update(Qr $qr)
    {

        $attributes = request()->validate(Qr::$rules, Qr::$messages);
        $subscriber = $this->updateEntry($qr, $attributes);
        return redirect_to_resource();
    }
    public function store()
    {
        $attributes = request()->validate(Qr::$rules, Qr::$messages);

        $subscriber = $this->createEntry(Qr::class, $attributes);

        return redirect_to_resource();
    }
    public function show(Qr $subscriber)
    {
        return $this->view('newsletters.subscribers.show')->with('item', $subscriber);
    }

}