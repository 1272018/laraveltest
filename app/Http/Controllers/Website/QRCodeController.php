<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Http\Requests;
use App\Models\Qr;
use Titan\Controllers\TitanWebsiteController;

class QRCodeController extends WebsiteController {

    public function check()
    {
        return $this->view('qr.check');
    }

    public function checkCode(Request $request){
        //$code=new QRcode();
        //$code->code=$request->code;
        //$code->save();
        // 1. Nhan code truyen len
        $code = $request->code;
        // 2. Query cai code trong db
        $codeData=Qr::where('code', $code)->first();
        //var_dump($codeData);
        // 3. Kiem tra ton taill
        if ($code=="$code") {
            // 3.1 - co -> thanh cong show san php_uname()
            return $this->view('qr.product_info')->with('message','dang nhap thanh cong ');
        } else {
            // 3.2 - ko co -> thong bao ko ton tai
            return redirect()->back()->with(['flag'=>'danger','message'=>'ma san pham khong ton tai']);
        }
    }

    public function checkCodeAjax(Request $request) {
        //$code=new QRcode();
        //$code->code=$request->code;
        //$code->save();
        // 1. Nhan code truyen len
        $code = $request->code;
        // 2. Query cai code trong db
        $codeData=Qr::where('code', $code)->first();
        //var_dump($codeData);
        // 3. Kiem tra ton taill
        if ($codeData) {
            // 3.1 - co -> thanh cong show san php_uname()
            echo json_encode($codeData);
        } else {
            // 3.2 - ko co -> thong bao ko ton tai
            $returnVal = [];
            $returnVal['error_message'] = "--Mã Code không tồn tại--";
            echo json_encode($returnVal);
        }
    }
}
