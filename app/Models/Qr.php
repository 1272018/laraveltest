<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Titan\Models\TitanCMSModel;

class Qr extends TitanCMSModel
{
    use SoftDeletes;
    protected $table = 'qr';
    protected $guarded = ['id'];
    static public $rules = [
        'code' => 'required|max:240',
    ];

}
