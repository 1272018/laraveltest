<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login'    => 'Đăng nhập',
    'register' => 'Đăng ký',
    'Pages'    => 'Trang',
    'Blog'    => 'Blog',
    'Gallery'    => 'Thư Viện',
    'News and Events'    => 'Tin Tức-Sự Kiên ',
    'About'    => 'Thông Tin',
    'Corporate'    => 'Công ty',
    'Contact Us'    => 'Liên Hệ',
    'My Account'    => 'Tài Khoản',
];
