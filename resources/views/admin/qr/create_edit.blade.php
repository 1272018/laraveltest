@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <span><i class="fa fa-edit"></i></span>
                        <span>{{ isset($item)? 'Edit the ' . $item->code: 'Create a new Qr code' }}</span>
                    </h3>
                </div>

                <div class="box-body no-padding">

                    @include('admin.partials.info')

                    <form method="POST" action="{{$selectedNavigation->url . (isset($item)? "/{$item->id}" : '')}}" accept-charset="UTF-8">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <input name="_method" type="hidden" value="{{isset($item)? 'PUT':'POST'}}">

                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ form_error_class('code', $errors) }}">
                                        <label for="code">Code</label>
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Please insert the Code" value="{{ ($errors && $errors->any()? old('code') : (isset($item)? $item->code : '')) }}">
                                        {!! form_error_message('code', $errors) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ form_error_class('product_id', $errors) }}">
                                        <label for="product_id">product_id</label>
                                        <input type="text" class="form-control" id="product_id" name="product_id" placeholder="Please insert the Code" value="{{ ($errors && $errors->any()? old('product_id') : (isset($item)? $item->product_id : '')) }}">
                                        {!! form_error_message('product_id', $errors) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ form_error_class('status', $errors) }}">
                                        <label for="status">Status</label>
                                        <input type="text" class="form-control" id="status" name="status" placeholder="Please insert the status" value="{{ ($errors && $errors->any()? old('status') : (isset($item)? $item->status : '')) }}">
                                        {!! form_error_message('status', $errors) !!}
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        @include('admin.partials.form_footer')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection