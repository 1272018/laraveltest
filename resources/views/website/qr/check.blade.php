@extends('layouts.website')
@section('styles')
    <style type="text/css">

        #v{
            width:325px;
        }
        #qr-canvas{
            display:none;
        }
        #qrfile{
            width:325px;

        }
        #mp1{
            text-align:center;
            font-size:35px;
        }
        #imghelp{
            position:relative;
            left:0px;
            top:-160px;
            z-index:100;
            font:18px arial,sans-serif;
            background:#f0f0f0;
            margin-left:35px;
            margin-right:35px;
            padding-top:10px;
            padding-bottom:10px;
            border-radius:20px;
        }

        #outdiv
        {
            align-content: center;
            width:330px;
            height:250px;
            border: solid;
            border-width: 3px 3px 3px 3px;
        }
        #result{
            border: solid;
            border-width: 1px 1px 1px 1px;
            padding:20px;
            width:100%;
        }

        #stt{
           
            border-width: 1px 1px 1px 1px;
            padding:10px;
            width:20%;
        }
        .tabs-menu{
            height: 30px;
            padding: 0;
            margin: 0;
        }
 
        .tabs-menu li {
            height: 30px;
            line-height: 30px;
            float: left;
            margin-right: 5px;
            border: 1px solid #d4d4d1;
            list-style-type: none;
        }
 
        .tabs-menu li.current {
            background-color: #007BB6;
        }
 
        .tabs-menu li a {
            padding: 10px;
            text-transform: uppercase;
            text-decoration: none; 
        }
 
        .tabs-menu .current a {
            color: #fff;
        }
 
        .tab {
            border: 1px solid #d4d4d1;
            float: left;
            width: 100%;
            margin-top: 20px;
            padding: 20px;
        }
 
        .tab-content {
            display: none;
        }
 
        #tab-1 {
            display: block;   
        }
        .myButton {
            background-color:gray;
            -moz-border-radius:10px;
            -webkit-border-radius:10px;
            border-radius:10px;
            border:2px solid #ffffff;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:Arial;
            font-size:15px;
            padding:5px 10px;
            text-decoration:none;
        }
            .myButton:hover {
                background-color:#00802b;
            }
        .myButton:active {
            position:relative;
            top:1px;
        }
    </style>
@endsection
@section('content')
<div class="row mt-5">
            <div class="col-lg-12">
                <h1 class="page-header" align="center">
                    Quét mã QR
                </h1>
            </div>
</div>
 <table style="width:100%" align="center"  >
    <tr>        
        <td>
            <table style="width:10%" align="center"  >
            <tr>
                <td>
                    <div id="mainbody">
                        <table class="tsel" width="100%"  >
                            <tr>
                                <td valign="top" align="center" width="50%">
                                    <table class="tsel" border="0">
                                        <tr>
                                            <td colspan="2" align="center">
                                                <div id="outdiv"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <canvas id="qr-canvas" width="800" height="600"></canvas>
                </td>
            </tr>
        </table>       
        <br/>
        <table align="center" >
            <tr >
                <td>
                    <input id="myFile" name="file" type="file" style="display: none" onchange="handleFiles(this.files)">
                    <button class="myButton" id="qrimg" onclick="$('#myFile').click()" >Browse</button>
                </td>
                <td width="185px"></td>
                <td align="center">
                    <button class="myButton"  id="webcamimg" onclick="setwebcam()">Camera</button>
                </td>
            </tr>
        </table>
        </br>
        <div align="center">
            <p style="background-color: #3a50d9; color: #e0eff2; font: italic bold 100px Georgia, Serif; text-shadow: -4px 3px 0 #3a50d9, -14px 7px 0 #0a0e27;">
            <h2>─────[ Hoặc ]─────</h2>
            </p>
        </div>     
             @if(Session::has('flag'))                   
                            <div align="center" class="alert alert-{{Session::get('flag')}}">
                                {{Session::get('message')}}
                            </div>
                @endif      
        </div>
        </br>
        <!-- Call to Action Section -->
        <table style="width:5%" align="center" >
            <tr>               
                <form id="target" action="{{ url('product_info') }}" method="POST" role="form">
                    {{ csrf_field()}}
                <td>
                    <input id="inputtext"  type="text" name="code" value="" size="28" placeholder="Nhập mã"  />
                </td>
                <td>
                    {{--<input type="submit" id="show-btn" name="Check" value="Check" />--}}
                    <input class="myButton" type="button" id="show-btn" name="Check" value="Check" onclick="checkCode()" />
                </td>
                </form>              
           </tr>
        </table>
        </td>
        <td width="50%" height="100%">
            <table width="100%">
                <tr>
                    <td colspan="2" height="110px" align="center"  valign="top">
                       <p style="text-align:center;color:green;background:#ffe6e6 ;padding: 10px">Thông tin sản phẩm
                       </p>
                     <div id="errorMessage" style="display: none" align="center" class="alert alert-error">
                     </td>
                </tr>
                <tr>
                    <td colspan="2" height="340px" align="center"  valign="top">                   
                        <div id="tabs-container">
                            <ul class="tabs-menu">
                                <li class="current"><a href="#tab-1">Mô tả</a></li>
                                <li><a href="#tab-2">Thông tin sản phẩm</a></li>
                                <li><a href="#tab-3">khuyến mãi</a></li>
                            </ul>
                            <div class="tab">
                                <div id="tab-1" class="tab-content">
                                 <p>chưa có dữ liệu</p>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <p>chưa có dữ liệu</p>            
                                </div>
                                <div id="tab-3" class="tab-content">
                                    <p>chưa có dữ liệu</p>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
@endsection
@section('scripts')
    <script type="text/javascript" src="/js/scr/llqrcode.js"></script>
    <script type="text/javascript" src="/js/scr/webqr.js?d=<?php echo date('YmdHis');?>"></script>
    @parent
    <script type="text/javascript" charset="utf-8">
        $(function () {
            if(isCanvasSupported() && window.File && window.FileReader) {
                initCanvas(800, 600);
                qrcode.callback = read;
                document.getElementById("mainbody").style.display="inline";
                setwebcam();
            } else {
                alert('sorry your browser is not supported')
            }

            $('#myFile').change(function() {
                $('#uploadFileForm').submit();
            });

            $('#form-newsletter-subscribe').submit(function () {
                FORM.sendFormToServer($(this), $(this).serialize());
                return false;
            });
           
        })
        function read(value) {
            $('#inputtext').val(value);
        }

        function checkCode() {
            // 1. lay gia tri trong input text
            var codeValue = $('#inputtext') .val();
            // 2. kiem tra gia tri trong input text
            if (codeValue != "") {
                // 3. Call Ajax
                $.ajax({
                    type: "POST",
                    url: "http://localhost:8080/checkCodeAjax",
                    data: {
                        code: codeValue
                    },
                    dataType: "json",
                    success: function (response) {
                        // 4. Show dataType
                        if (response.error_message) {
                            var errorElem = $('#errorMessage');
                            errorElem.html(response.error_message);
                            errorElem.show();
                        } else {
                            $('#errorMessage').hide();
                            // show data
                        }
                    }
                });
            } else {
                // show error
                var errorElem = $('#errorMessage');
                errorElem.html("---Vui lòng nhập mã Code---");
                errorElem.show();
            }
        }

    </script>
     <script type="text/javascript" charset="utf-8">
        $("div.alert").delay(3000).slideUp();
        $('#errorMessage').delay(3000).slideUp();
     </script>
     
    <script>
        $(document).ready(function() {
            $(".tabs-menu a").click(function(e) {
                e.preventDefault();
                $(this).parent().addClass("current");
                $(this).parent().siblings().removeClass("current");
                var tab = $(this).attr("href");
                $(".tab-content").not(tab).css("display", "none");
                $(tab).fadeIn();
            });
        });
    </script>
@endsection